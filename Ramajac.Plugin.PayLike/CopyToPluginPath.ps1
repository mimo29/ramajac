 param (
    [string]$build = "debug",
    [string]$password = $( Read-Host "Input password, please" ),
    [switch]$force = $false
 )

$scriptDir = Split-Path -Parent $MyInvocation.MyCommand.Path
$binDir  = Join-Path -Path $scriptDir -ChildPath "Ramajac.Plugin.PayLike\bin\$build\*"

Copy-Item -Filter "Ramajac.Plugin.PayLike*" -Path $binDir -Destination "c:\Uniconta\PluginPath\" -force