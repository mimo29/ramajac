﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.DataModel;

namespace Ramajac.Plugin.PayLike.Models
{

	public class PayLikeLog : TableData
	{
		public override int UserDefinedId { get { return 502; } }
		public string Debtor
		{
			get { return this.GetUserFieldString(0); }
			set { this.SetUserFieldString(0, value); }
		}

		public string InvoiceNumber
		{
			get { return this.GetUserFieldString(1); }
			set { this.SetUserFieldString(1, value); }
		}

		public string TransactionId
		{
			get { return this.GetUserFieldString(2); }
			set { this.SetUserFieldString(2, value); }
		}

		public DateTime InvoiceDate
		{
			get { return this.GetUserFieldDateTime(3); }
			set { this.SetUserFieldDateTime(3, value); }
		}

		public DateTime DueDate
		{
			get { return this.GetUserFieldDateTime(4); }
			set { this.SetUserFieldDateTime(4, value); }
		}

		public bool SentToPaylike
		{
			get { return this.GetUserFieldBoolean(5); }
			set { this.SetUserFieldBoolean(5, value); }
		}

		public string TransactionResponse
		{
			get { return this.GetUserFieldString(6); }
			set { this.SetUserFieldString(6, value); }
		}

		public bool Redeemed
		{
			get { return this.GetUserFieldBoolean(7); }
			set { this.SetUserFieldBoolean(7, value); }
		}

		public DateTime RedeemedDate
		{
			get { return this.GetUserFieldDateTime(8); }
			set { this.SetUserFieldDateTime(8, value); }
		}

		public DateTime SentToPaylikeDate
		{
			get { return this.GetUserFieldDateTime(9); }
			set { this.SetUserFieldDateTime(9, value); }
		}
	}
}
