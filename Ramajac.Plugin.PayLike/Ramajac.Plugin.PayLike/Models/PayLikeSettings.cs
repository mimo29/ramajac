﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.DataModel;

namespace Ramajac.Plugin.PayLike.Models
{
	public class PayLikeSettings : TableData
	{
		public override int UserDefinedId { get { return 501; } }
		public string MerchantId
		{
			get { return this.GetUserFieldString(0); }
			set { this.SetUserFieldString(0, value); }
		}

		public string AppKey
		{
			get { return this.GetUserFieldString(1); }
			set { this.SetUserFieldString(1, value); }
		}

		public string PayLikeDebtor
		{
			get { return this.GetUserFieldString(2); }
			set { this.SetUserFieldString(2, value); }
		}

		public string PayLikeFeeDebtor
		{
			get { return this.GetUserFieldString(3); }
			set { this.SetUserFieldString(3, value); }
		}

		public string DailyJournalName
		{
			get { return this.GetUserFieldString(4); }
			set { this.SetUserFieldString(4, value); }
		}
		public DateTime LastRun
		{
			get { return this.GetUserFieldDateTime(5); }
			set { this.SetUserFieldDateTime(5, value); }
		}


	}
}
