﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.ClientTools.DataModel;

namespace Ramajac.Plugin.PayLike.Models
{
  public class DebtorClientUser : DebtorClient
  {
    public string PaylikeTransactionId
    {
      get { return this.GetUserFieldString(GetUserFieldIndex("PaylikeTransactionId")); }
      set { this.SetUserFieldString(GetUserFieldIndex("PaylikeTransactionId"), value); }
    }

  }

}
