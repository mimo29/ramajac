﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlexDev.Core;
using Uniconta.API.System;
using Ramajac.Plugin.PayLike.Models;
using Uniconta.Common;

namespace Ramajac.Plugin.Paylike.Repositories
{
  public class ConfigurationRepository
  {
    private readonly SessionContext _sessionContext;
    public PayLikeSettings _config;
    private static List<PayLikeLog> _log;

    public ConfigurationRepository(SessionContext sessionContext)
    {
      _sessionContext = sessionContext;
    }

		public PayLikeSettings Load()
		{
			return _config = _sessionContext.Api.Query.QuerySync(new PayLikeSettings(), null, null).FirstOrDefault();
		}
		public List<PayLikeLog> LoadLog()
		{
			return _log = _sessionContext.Api.Query.QuerySync(new PayLikeLog(), null, null).ToList();
		}
		public List<PayLikeLog> LoadLog(IEnumerable<PropValuePair> propValue)
		{
			return _log = _sessionContext.Api.Query.QuerySync(new PayLikeLog(), null, propValue).ToList();
		}
	}
}
