﻿using System;
using System.Collections.Generic;
using System.Linq;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;
using Paylike.NET.RequestModels.Transactions;
using Paylike.NET.Constants;
using Paylike.NET.Interfaces;
using Paylike.NET;
using FlexDev.Core;
using Ramajac.Plugin.PayLike.Models;
using Uniconta.DataModel;
using System.Windows.Forms;

namespace Ramajac.Plugin.PayLike.Services
{
	public class TransactionService : ServiceBase
	{
		public TransactionService(SessionContext session) : base(session)
		{
		}

    public ErrorCodes CreateAndCaptureTransactionTest(SessionContext context, IEnumerable<DebtorInvoiceClient> debtorInvoiceClients)
    {
      ErrorCodes result = ErrorCodes.Succes;
      DateTime lastRun = Config.Load().LastRun;
      string errorDescription = string.Empty;



      List<DebtorInvoiceClient> newDebtorInvoiceList = debtorInvoiceClients.Where(c => c.Payment == "Kreditkort").OrderBy(nd => nd.InvoiceNumber).ToList();
      List<long> InvoiceNumbers = newDebtorInvoiceList.Select(i => i.InvoiceNumber).ToList();

      ////////////////////////////////////////// DISTINCT TEST
      var selectLogsInv2 = PropValuePair.GenereteWhereElements(nameof(PayLikeLog.InvoiceNumber), typeof(string), string.Join(";", InvoiceNumbers));
      var payLikeLog2 = Config.LoadLog(new[] { selectLogsInv2 });
      payLikeLog2 = payLikeLog2.OrderBy(ds => ds.InvoiceNumber).ToList();
      ////////////////////////////////////////// DISTINCT TEST
      
      //var selectLogsInv = PropValuePair.GenereteWhereElements(nameof(PayLikeLog.InvoiceNumber), typeof(string), string.Join(";", InvoiceNumbers));
      var payLikeLog = Config.LoadLog().OrderBy(s => s.InvoiceNumber).ToList();

      //////////////////////////////////////////// TEST FILTER
      payLikeLog = payLikeLog.Where(f => InvoiceNumbers.Any(g => g.ToString() == f.InvoiceNumber) ).ToList();
      //////////////////////////////////////////// TEST FILTER


      List<string> payLikeOk = payLikeLog.Where(pl => pl.TransactionResponse == "PostOk").Select(pll => pll.InvoiceNumber).ToList();

      if (payLikeLog != null && payLikeLog.Count > 0)
      {
        newDebtorInvoiceList = newDebtorInvoiceList.Where(ic => !payLikeOk.Any(pll =>  pll == ic.InvoiceNumber.ToString())).ToList();
        MessageBox.Show(newDebtorInvoiceList.Count.ToString() + " betalinger ville blive opkrævet");
      }
      return result;
    }

    public ErrorCodes CreateAndCaptureTransaction(SessionContext context, IEnumerable<DebtorInvoiceClient> debtorInvoiceClients)
		{
			ErrorCodes result = ErrorCodes.Succes;
			DateTime lastRun = Config.Load().LastRun;
			string errorDescription = string.Empty;
			if (lastRun.AddMinutes(5) > DateTime.Now)
			{
				return ErrorCodes.NoSucces;
			}
			else
			{
				Config._config.LastRun = DateTime.Now;
				Session.Api.Crud.UpdateNoResponse(Config._config);
			}
      List<DebtorInvoiceClient> newDebtorInvoiceList = debtorInvoiceClients.Where(c => c.Payment == "Kreditkort").OrderBy(nd => nd.InvoiceNumber).ToList();

      IPaylikeTransactionService payLikeTransactionService = new PaylikeTransactionService(Config.Load().AppKey);
			CreateTransactionRequest createTransactionRequest;
			List<long> InvoiceNumbers = newDebtorInvoiceList.Select(i => i.InvoiceNumber).ToList();

      // get logged completed DebtorInvoices
			var payLikeLog =  Config.LoadLog().OrderBy(pl => pl.InvoiceNumber).ToList();
      if (payLikeLog != null && payLikeLog.Count > 0)
      {
        payLikeLog = payLikeLog.Where(f => InvoiceNumbers.Any(g => g.ToString() == f.InvoiceNumber)).ToList();
        List<string> payLikeOk = payLikeLog.Where(pl => pl.TransactionResponse == "PostOk").Select(pll => pll.InvoiceNumber).ToList();

        newDebtorInvoiceList = newDebtorInvoiceList.Where(ic => !payLikeOk.Any(pll => pll == ic.InvoiceNumber.ToString())).ToList();

        DialogResult res = MessageBox.Show(newDebtorInvoiceList.Count.ToString() + " betalinger bliver opkrævet. Fortsæt?", "Attention!", MessageBoxButtons.OKCancel);

        if (res == DialogResult.OK)
        {
          foreach (var debtorInvoiceClient in newDebtorInvoiceList)
          {
            bool error = false;
            ResponseCode transactionResult = ResponseCode.UnknownResponseCode;
            // hent debtor
            PropValuePair select = PropValuePair.GenereteWhereElements(nameof(DebtorClient.Account), typeof(string), debtorInvoiceClient.Account);
            var debtorClientUser = Session.QuerySync<DebtorClientUser>(select)[0];
            Dictionary<string, string> custom = new Dictionary<string, string>() { { "Fakturanummer: ", debtorInvoiceClient.InvoiceNumber.ToString() } };

            // opret ny transaktion fra gammel transaktion
            createTransactionRequest = new CreateTransactionRequest()
            {
              Amount = Convert.ToInt32(debtorInvoiceClient.TotalAmount * 100),
              Currency = Currency.DKK,
              Descriptor = "pudserklubben",
              CardId = debtorClientUser.PaylikeTransactionId,
              MerchantId = Config.Load().MerchantId,
              Custom = custom
            };



            var response = payLikeTransactionService.CreateTransaction(createTransactionRequest);
            if (Enum.IsDefined(typeof(ResponseCode), response.ResponseCode))
            {
              transactionResult = (ResponseCode)response.ResponseCode;
            }
            if (response.IsError)
            {
              errorDescription = response.ErrorMessage;
            }
            // capture ny transaktion
            if (!response.IsError && response.ResponseCode == (int)ResponseCode.PostOk && !string.IsNullOrEmpty(response.Content.Id))
            {
              CaptureTransactionRequest captureRequest = new CaptureTransactionRequest()
              {
                Amount = createTransactionRequest.Amount,
                Currency = createTransactionRequest.Currency,
                TransactionId = response.Content.Id
              };

              var captureResponse = payLikeTransactionService.CaptureTransaction(captureRequest);
              if (Enum.IsDefined(typeof(ResponseCode), captureResponse.ResponseCode))
              {
                transactionResult = (ResponseCode)captureResponse.ResponseCode;
              }
              if (response.IsError)
              {
                errorDescription = response.ErrorMessage;
              }
              var transaction = captureResponse.Content;
              if (captureResponse.IsError || captureResponse.ResponseCode != (int)ResponseCode.PostOk || !transaction.Trail[0].Capture)
              {
                // log error in capture
                PayLikeLog log1 = new PayLikeLog()
                {
                  Debtor = debtorClientUser.Account,
                  InvoiceNumber = debtorInvoiceClient.InvoiceNumber.ToString(),
                  TransactionResponse = transactionResult.ToString() + ": " + errorDescription,
                  SentToPaylikeDate = DateTime.Now,
                };
                Session.Api.Crud.InsertNoResponse(log1);
                error = true;
                //return ErrorCodes.Exception;
              }
            }
            else
            {
              // log transaction creation error
              PayLikeLog log2 = new PayLikeLog()
              {
                Debtor = debtorClientUser.Account,
                InvoiceNumber = debtorInvoiceClient.InvoiceNumber.ToString(),
                TransactionResponse = transactionResult.ToString() + ": " + errorDescription,
                SentToPaylikeDate = DateTime.Now
              };
              Session.Api.Crud.InsertNoResponse(log2);
              error = true;
              //return ErrorCodes.Exception;
            }
            if (!error)
            {
              // log successful resultat
              PayLikeLog log3 = new PayLikeLog()
              {
                Debtor = debtorClientUser.Account,
                DueDate = debtorInvoiceClient.DueDate ?? DateTime.Now,
                InvoiceNumber = debtorInvoiceClient.InvoiceNumber.ToString(),
                InvoiceDate = debtorInvoiceClient._Date,
                Redeemed = true,
                RedeemedDate = DateTime.Now,
                SentToPaylike = true,
                SentToPaylikeDate = DateTime.Now,
                TransactionId = response.Content.Id,
                TransactionResponse = transactionResult.ToString()
              };
              Session.Api.Crud.InsertNoResponse(log3);

              // Opret kladdelinjer i bogføring
              // hent først kladde
              select = PropValuePair.GenereteWhereElements(nameof(GLDailyJournalClient.Journal), typeof(string), Config.Load().DailyJournalName);
              var journal = Session.QuerySync<GLDailyJournalClient>(select).FirstOrDefault();

              GLDailyJournalLineClient linje = new GLDailyJournalLineClient();

              CopyJournalValues(journal, linje);

              linje._Account = debtorClientUser.Account;
              //linje.AccountType = "Debitor";
              linje.Amount = (-1) * debtorInvoiceClient.TotalAmount;
              linje.Date = DateTime.Now;
              //linje.OffsetAccount = Config.Load().PayLikeDebtor;
              linje.Invoice = debtorInvoiceClient.InvoiceNumber;
              linje.Text = "Fakturanummer " + debtorInvoiceClient.InvoiceNumber;
              //linje.OffsetAccountType = "Debitor";

              Session.Api.Crud.InsertNoResponse(linje);
            }
          }
        }
      }
			return result;
		}

		protected static void CopyJournalValues(GLDailyJournalClient @from, GLDailyJournalLineClient to)
		{
			to._AccountType = (byte)GLJournalAccountType.Debtor;
			to._Account = @from._Account;
			to._OffsetAccountType = (byte)@from._DefaultOffsetAccountType;
			to._OffsetAccount = @from._OffsetAccount;
			if (to._TransType == null)
				to._TransType = @from._TransType;


			//to._Vat = @from._Vat;
			//to._OffsetVat = @from._OffsetVat;

			to._Dim1 = @from._Dim1;
			to._Dim2 = @from._Dim2;
			to._Dim3 = @from._Dim3;
			to._Dim4 = @from._Dim4;
			to._Dim5 = @from._Dim5;

			to._SettleValue = @from._SettleValue;
			to.SetMaster(@from);
		}
	}
}
