﻿using FlexDev.Core;
using Ramajac.Plugin.Paylike.Repositories;
using Ramajac.Plugin.PayLike.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ramajac.Plugin.PayLike.Services
{
	public class ServiceBase
	{
		protected ServiceBase(SessionContext session)
		{
			Session = session;
			Config = new ConfigurationRepository(Session);
			Log = Config.LoadLog();
		}

		protected ConfigurationRepository Config { get; }

		protected SessionContext Session { get; }

		protected List<PayLikeLog> Log { get; }
	}
}
