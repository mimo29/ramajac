﻿using FlexDev.Core;
using Ramajac.Plugin.PayLike.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniconta.API.Plugin;
using Uniconta.API.Service;
using Uniconta.ClientTools.DataModel;
using Uniconta.Common;
using Uniconta.DataModel;

namespace Ramajac.Plugin.PayLike
{
	public class PayLikePlugin : IPluginBase
	{
		private BaseAPI _api;
		public string Name => "PayLikePlugin";
		private string _errorDescription = string.Empty;

		public event EventHandler OnExecute;

		public ErrorCodes Execute(UnicontaBaseEntity master, UnicontaBaseEntity currentRow, IEnumerable<UnicontaBaseEntity> source, string command, string args)
		{
			ErrorCodes errorCode = ErrorCodes.Succes;
			var context = new SessionContext(_api);

			switch (command)
			{
				case "CreateAndCaptureTransactions":
					if (currentRow.BaseEntityType() == typeof(DebtorInvoice))
					{
						errorCode = CreateAndCaptureTransaction(context, source.Cast<DebtorInvoiceClient>());
						if(errorCode == ErrorCodes.NoSucces)
						{
							_errorDescription = "Det er under 5 min. siden sidste kørsel. Vent venligst.";
						}
					}
					break;
        case "CreateAndCaptureTransactionsTest":
          if (currentRow.BaseEntityType() == typeof(DebtorInvoice))
          {
            errorCode = CreateAndCaptureTransactionTest(context, source.Cast<DebtorInvoiceClient>());
            if (errorCode == ErrorCodes.NoSucces)
            {
              _errorDescription = "Det er under 5 min. siden sidste kørsel. Vent venligst.";
            }
          }
          break;
      }
			return errorCode;
		}

		private ErrorCodes CreateAndCaptureTransaction(SessionContext context, IEnumerable<DebtorInvoiceClient> debtorInvoiceClients)
		{
			TransactionService transactionService = new TransactionService(context);
			return transactionService.CreateAndCaptureTransaction(context, debtorInvoiceClients);
		}

    private ErrorCodes CreateAndCaptureTransactionTest(SessionContext context, IEnumerable<DebtorInvoiceClient> debtorInvoiceClients)
    {
      TransactionService transactionService = new TransactionService(context);
      return transactionService.CreateAndCaptureTransactionTest(context, debtorInvoiceClients);
    }

    public string[] GetDependentAssembliesName()
		{
			return new string[0];
		}

		public string GetErrorDescription()
		{
			return _errorDescription;
		}

		public void Intialize()
		{

		}

		public void SetAPI(BaseAPI api)
		{
			_api = api;
		}

		public void SetMaster(List<UnicontaBaseEntity> masters)
		{

		}
	}
}
